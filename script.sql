DROP DATABASE IF EXISTS parc_electrique;
CREATE DATABASE IF NOT EXISTS parc_electrique CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE parc_electrique;

CREATE OR REPLACE TABLE Vehicule(
    id integer AUTO_INCREMENT primary key,
    plaque varchar(30) unique not null
);

INSERT INTO Vehicule VALUES('','AA-000-AA');
INSERT INTO Vehicule VALUES('','111 BBB 11');


CREATE OR REPLACE TABLE Utilisateur(
   id integer AUTO_INCREMENT PRIMARY key,
   nom varchar(30) not null,
   prenom varchar(30) not null,
   mdp varchar(30) not null,
   adresse varchar(30) not null,
   mobile varchar(30) not null,
   mail varchar(30) unique not null,
    
   type enum("client","abonne","exploitant")
    
);

INSERT INTO Utilisateur (nom, prenom, mdp, adresse, mobile, mail, type) VALUES('Dillenschneider','Ernesto','mdp','adresse','06060606','ernesto@gmail.com','exploitant');
INSERT INTO Utilisateur (nom, prenom, mdp, adresse, mobile, mail, type)  VALUES('Samson','Lorrain','mdp','adresse','06060606','lorrain@gmail.com','abonne');
INSERT INTO Utilisateur (nom, prenom, mdp, adresse, mobile, mail, type)  VALUES('Poirot','Cloé','mdp','adresse','06060606','cloe@gmail.com','client');

CREATE OR REPLACE TABLE Vehicule_Utilisateur(
    idVehicule integer,
    idUtilisateur integer,
    primary key(idVehicule,idUtilisateur)
);


ALTER TABLE Vehicule_Utilisateur ADD CONSTRAINT FK_Vehicule FOREIGN KEY (idVehicule) REFERENCES Vehicule(id);
ALTER TABLE Vehicule_Utilisateur ADD CONSTRAINT FK_Utilisateur FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(id);

insert into vehicule_utilisateur(idUtilisateur,idVehicule) values(2,1);
insert into vehicule_utilisateur(idUtilisateur,idVehicule) values(3,2);

CREATE OR REPLACE TABLE Reservation(
    idresa integer AUTO_INCREMENT primary key,
    dateDebut datetime not null,
    dateFin datetime not null,
    prolongation integer default '0' CHECK (prolongation < 3),
    idBorne integer not null,
    idUtilisateur integer not null
);
ALTER TABLE Reservation ADD CONSTRAINT FK_Utilisateur_Reservation FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(id);
ALTER TABLE Reservation ADD CONSTRAINT FK_Borne_Reservation FOREIGN KEY (idBorne) REFERENCES Borne(id);

CREATE OR REPLACE TABLE Frais(
    idfr integer AUTO_INCREMENT primary key,
    montant double not null,
    idRes integer not null,
    typeFrais ENUM("normal","depassement","penalite","contrat")
);
ALTER TABLE Frais ADD CONSTRAINT FK_Reservation FOREIGN KEY (idRes) REFERENCES Reservation(idresa);

CREATE OR REPLACE TABLE Borne(
    id integer AUTO_INCREMENT primary key,
    etat ENUM('disponible','indisponible') not null
);

INSERT INTO Borne VALUES('','disponible');
INSERT INTO Borne VALUES('','indisponible');


CREATE OR REPLACE TABLE Contrat(
    id integer AUTO_INCREMENT primary key,
    idBorne integer not null,
    idUtilisateur integer not null,
    idReservation integer not null
);

ALTER TABLE Contrat ADD CONSTRAINT FK_Borne_Contrat FOREIGN KEY (idBorne) REFERENCES Borne(id);
ALTER TABLE Contrat ADD CONSTRAINT FK_Utilisateur_Contrat FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(id);
ALTER TABLE Contrat ADD CONSTRAINT FK_Reservation_Contrat FOREIGN KEY (idReservation) REFERENCES Reservation(idresa);

CREATE OR REPLACE TABLE Archives(
    id integer AUTO_INCREMENT primary key,
    idRes integer null,
    dateDebut datetime null,
    dateFin datetime null,
    idUtilisateurReservation int null,
    idFrais integer null,
    typeFrais ENUM("normal","depassement","penalite","contrat") not null,
    montant double not null,
    idContrat integer null,
    idUtilisateurContrat int null
);
--ALTER TABLE Archives ADD CONSTRAINT FK_Reservation_Archive FOREIGN KEY (idRes) REFERENCES Reservation(id);
--ALTER TABLE Archives ADD CONSTRAINT FK_Frais_Archive FOREIGN KEY (idFrais) REFERENCES Frais(id);
--ALTER TABLE Archives ADD CONSTRAINT FK_Contrat_Archive FOREIGN KEY (idContrat) REFERENCES Contrat(id);

