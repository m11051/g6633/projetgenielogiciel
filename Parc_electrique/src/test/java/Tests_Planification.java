import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import modeles.M_Planification;
import classes_techniques.Borne;
import classes_techniques.EtatBorne;
import classes_techniques.Reservation;
import classes_techniques.TypeUtilisateur;
import classes_techniques.Utilisateur;
import modeles.M_Bdd;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Connection;
import java.time.LocalDateTime;

public class Tests_Planification {

	Borne b=new Borne(EtatBorne.indisponible);
	Utilisateur u=new Utilisateur(1,"Jean","Pierre","13 rue du moulin","0612345678","jpp@gmail.com",TypeUtilisateur.client);
	
	@Test
	void TestRetardDansPA() {
		Reservation r=new Reservation(LocalDateTime.now().minusMinutes(10),LocalDateTime.now().plusMinutes(120),b,u);
		assertThat(M_Planification.retardDansPA(r)).isTrue();
	}
	
	@Test
	void TestRetardDepassePA() {
		Reservation r=new Reservation(LocalDateTime.now().minusMinutes(30),LocalDateTime.now().plusMinutes(120),b,u);
		assertThat(M_Planification.retardSuperieurPA(r)).isTrue();
	}
	
	@Test
	void TestArriveeAHeure() {
		Reservation r=new Reservation(LocalDateTime.now().minusMinutes(2),LocalDateTime.now().plusMinutes(120),b,u);
		assertThat(M_Planification.arriveALHeure(r)).isTrue();
	}
	
	@Test
	void TestArriveNonALHeure() {
		Reservation r=new Reservation(LocalDateTime.now().minusMinutes(7),LocalDateTime.now().plusMinutes(120),b,u);
		assertThat(M_Planification.arriveALHeure(r)).isFalse();
	}

}
