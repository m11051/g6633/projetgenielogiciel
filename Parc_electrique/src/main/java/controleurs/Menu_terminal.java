package controleurs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import classes_techniques.Archive;
import classes_techniques.Borne;
import classes_techniques.Contrat;
import classes_techniques.EtatBorne;
import classes_techniques.Frais;
import classes_techniques.Reservation;
import classes_techniques.TypeFrais;
import classes_techniques.TypeUtilisateur;
import classes_techniques.Utilisateur;
import modeles.M_Gestion;
import modeles.M_Planification;
import modeles.M_Reservation;

import static modeles.M_Reservation.*;

public class Menu_terminal {

	public static void main(String[] args) throws IOException, ParseException {

		// Déclaration de variables
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		int choixEntree;
		int choixnonconnecte;
		int choixconnecte;
		Utilisateur currentuser = null;
		boolean quit = false;

		// Affichage du menu :
		while (quit == false) {
			if (currentuser == null) {

				System.out.println("1 : Inscription");
				System.out.println("2 : Connexion par le mail");
				System.out.println("3 : Connexion par la plaque");
				System.out.println("4 : Quitter le terminal");
				choixEntree = M_Planification.saisieInt();
				switch (choixEntree) {

				case 1:
					String nom = "";
					while (nom.length() < 1) {
						System.out.println("Saisissez votre nom");
						nom = in.readLine();
					}
					String prenom = "";
					while (prenom.length() < 1) {
						System.out.println("Saisissez votre prénom");
						prenom = in.readLine();
					}
					String adresse = "";
					while (adresse.length() < 1) {
						System.out.println("Saisissez votre adresse");
						adresse = in.readLine();
					}
					String mdp = "";
					while (mdp.length() < 3) {
						System.out.println("Saisissez votre mot de passe");
						mdp = in.readLine();
					}
					String mobile = "";
					while (mobile.length() < 1 || mobile.length() > 10) {
						System.out.println("Saisissez votre numéro de téléphone");
						mobile = in.readLine();
					}
					String mail = "";
					while (mail.length() < 1) {
						System.out.println("Saisissez votre mail");
						mail = in.readLine();
					}
					currentuser = M_Gestion.inscription(nom, prenom, adresse, mdp, mobile, mail);
					break;
				case 2:
					// System.out.println("Le numéro de plaque est inconnu, veuillez vous identifier
					// :");
					System.out.println("Veuillez entrer votre email :");
					mail = in.readLine();
					System.out.println("Entrez votre mot de passe :");
					mdp = in.readLine();
					currentuser = M_Gestion.connexion(mail, mdp);
					// L'utilisateur saisit des données de connexion invalides
					if (currentuser == null) {
						System.out.println(
								"Les identifiants saisis n'existent pas, veuillez vous inscrire sur l'application mobile pour pouvoir vous connecter");
					}
					break;
				case 3:
					System.out.println("Veuillez entrer votre numéro de plaque : (stop pour arrêter)");
					String plaque = "";
					plaque = in.readLine();
					while (!verifImmat(plaque) && !plaque.equals("stop")) {
						plaque = in.readLine();
					}
					if (verifImmat(plaque)) {
						currentuser = M_Planification.RetournerProprietaireVehicule(plaque);
						if (currentuser == null)
							System.out.println("Plaque inconnue");
					}

					break;
				case 4:
					quit = true;
					break;
				}
			} else {
				Reservation resaActuelle = M_Planification.getIdReservationActuelle(currentuser);
				currentuser.setVehicules(M_Gestion.retournerVehicules(currentuser));
				currentuser.setReservations(M_Gestion.retournerReservation(currentuser));
				currentuser.setContrats(M_Gestion.retournerContrats(currentuser));
				System.out.println("Bienvenue " + currentuser.getPrenom() + " !");
				System.out.println("-----------------------------------------");
				System.out.println("1 : Arrivée sur une borne");
				System.out.println("2 : Gérer mes réservations");
				System.out.println("3 : Gérer les véhicules");
				System.out.println("4 : Départ d'une borne");
				System.out.println("5 : S'abonner");
				if (currentuser.getTypeutil().equals(TypeUtilisateur.abonne)
						|| currentuser.getTypeutil().equals(TypeUtilisateur.exploitant))
					System.out.println("6 : Se désabonner");
				if (currentuser.getTypeutil().equals(TypeUtilisateur.exploitant))
					System.out.println("7 : Voir les profils clients");
				if (currentuser.getTypeutil().equals(TypeUtilisateur.exploitant))
					System.out.println("8 : Modifier prix générique");
				System.out.println("9 : Quitter le terminal");
				System.out.println("10 : Se déconnecter");
				System.out.println("11 : Vue bornes indisponibles");
				choixconnecte = M_Planification.saisieInt();
				switch (choixconnecte) {
				case 1:
					if (resaActuelle == null) {
						System.out.println(
								"Vous n'avez pas de réservation en cours actuellement, veuillez créer une reservation.");
						break;
					}
					if (resaActuelle.getBorne().getEtat().equals(EtatBorne.indisponible)) {
						System.out.println(
								"La borne qui vous a été attribuée n'est plus disponible actuellement car le client précédent a dépassé son créneau.");
						List<Borne> bornesDispos = M_Planification.recupererBornesDispos(LocalDateTime.now(),
								resaActuelle.getDateFin());
						if (bornesDispos.isEmpty()) {
							System.out.println(
									"Il n'y a plus de bornes disponibles actuellement, veuillez nous excuser pour la gène occasionnée.");
						} else {
							M_Planification.updateBorneResa(resaActuelle, bornesDispos.get(0),
									resaActuelle.getDateFin());

							System.out.println("Vous pouvez vous brancher à la borne n°" + bornesDispos.get(0).getId()
									+ ", bonne journée");
						}
					}

					if (M_Planification.arriveALHeure(resaActuelle)) {
						System.out.println("Vous êtes à l'heure, vous pouvez brancher votre voiture à la borne n°"
								+ resaActuelle.getBorne().getId() + ", Bonne journée !");
					} else if (M_Planification.retardDansPA(resaActuelle)) {
						System.out.println("Vous avez plus de 5 minutes de retard, la date de fin est de : "
								+ resaActuelle.getDateFin() + "\nvoulez vous prolonger la recharge ?");
						System.out.println("1 : Oui");
						System.out.println("2 : Non");
						int choix = M_Planification.saisieInt();
						switch (choix) {
						case 1:
							System.out.println("Entrez la durée de prolongement (en minutes) :");
							int duree = M_Planification.saisieInt();
							if (M_Planification.verifBorneDispoPourProlong(resaActuelle.getBorne(), resaActuelle,
									duree)) {
								M_Planification.prolongerDureeReservation(resaActuelle.getId(), duree);
								System.out
										.println("La reservation a été étendue, la date de fin est maintenant prévue à "
												+ resaActuelle.getDateFin());
							} else {
								System.out.println(
										"La borne est réservée par un autre client sur ce créneau, une prolongation n'est donc pas possible");
							}
							break;
						case 2:
							System.out.println("La date de fin reste inchangée, bonne journée");
							break;
						}
					} else if (M_Planification.retardSuperieurPA(resaActuelle)) {
						System.out.println(
								"Vous êtes en retard de plus de 15 minutes, pour éviter d'avoir des places vides inutilement dans le parking nous avons remis votre borne initiale en état disponible");
						System.out.println(
								"Voulez vous augmenter la durée de stationnement en conséquence du retard ? (fin prévue :"
										+ resaActuelle.getDateFin() + ")");
						System.out.println("1 : Oui");
						System.out.println("2 : Non");
						int choix = M_Planification.saisieInt();
						switch (choix) {
						case 1:
							System.out.println("Entrer la durée que vous souhaitez ajouter :");
							int duree = M_Planification.saisieInt();
							resaActuelle.setDateFin(resaActuelle.getDateFin().plusMinutes(duree));
							M_Planification.prolongerDureeReservation(resaActuelle.getId(), duree);
							System.out.println("La nouvelle date de fin est prévue pour :" + resaActuelle.getDateFin());
							break;
						case 2:
							System.out.println("La date de fin reste inchangée.");
							break;
						}
						List<Borne> bornesDispos = M_Planification.recupererBornesDispos(LocalDateTime.now(),
								resaActuelle.getDateFin());
						if (bornesDispos.isEmpty()) {
							System.out.println(
									"Il n'y a plus de bornes disponibles actuellement, veuillez nous excuser pour la gène occasionnée.");
						} else {
							M_Planification.updateBorneResa(resaActuelle, bornesDispos.get(0),
									resaActuelle.getDateFin());

							System.out.println("Vous pouvez vous brancher à la borne n°" + bornesDispos.get(0).getId()
									+ ", bonne journée");
						}
					}
					break;
				case 2:
					viewReservations(currentuser);
					System.out.println("1 : Réservation immédiate rapide");
					
					int choix=in.read();
					switch (choix) {
					case 1:
						System.out.println("Entrez la durée de la réservation (en heures):");
						int duree=in.read();
						List<Borne> bornesdispos=M_Planification.recupererBornesDispos(LocalDateTime.now(), LocalDateTime.now().plusHours(duree));
						if(bornesdispos.isEmpty()) {
							System.out.println("Aucune borne n'est actuellement disponible, veuillez nous excuser pour la gène occasionnée");
						}
						else {
							M_Planification.reservationSurBorne(duree,bornesdispos.get(0),currentuser);
							System.out.println("Vous avez placé une réservation avec succès, vous pouvez aller vous brancher à la borne n°"+bornesdispos.get(0).getId());
						}
						break;
					}
					// viewReservations(currentuser, plaque);
					break;
				case 3:
					// System.out.println("4 : Ajouter un véhicule");
					currentuser.afficheVehicules();
					break;
				case 4:
					System.out.println("Merci d'avoir utilisé nos services et au revoir !");
					M_Planification.depart(resaActuelle);
					break;
				case 5:
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime dateDebut = null;
					LocalDateTime dateFin = null;

					boolean pass = false;
					String debut = "";
					while (!pass) {
						System.out.println("Saisissez une date de debut (yyyy-MM-dd HH:mm:ss)");
						debut = in.readLine();
						try {
							dateDebut = LocalDateTime.parse(debut, formatter);
							pass = true;
						} catch (DateTimeParseException e) {

						}
					}

					pass = false;
					String fin = "";
					while (!pass) {
						System.out.println("Saisissez une date de fin (yyyy-MM-dd HH:mm:ss)");
						fin = in.readLine();
						try {
							dateFin = LocalDateTime.parse(fin, formatter);
							if (dateFin.isAfter(dateDebut))
								pass = true;
							else
								System.out.println("Date de fin antérieure à la date de début");
						} catch (DateTimeParseException e) {

						}
					}

					int i = 0;
					List<Borne> bornes = M_Planification.recupererBornesDispos(dateDebut, dateFin);
					for (Borne b : bornes) {
						System.out.println(i + " " + b.toString());
						i++;
					}
					if (bornes.size() > 0) {

						int borneSelected = -1;
						while (borneSelected < 0 || borneSelected > bornes.size() - 1) {
							System.out.println("Sélectionnez une des bornes disponibles");
							borneSelected = M_Planification.saisieInt();
						}

						Reservation reservation = new Reservation(dateDebut, dateFin, bornes.get(borneSelected),
								currentuser);
						Contrat contrat = new Contrat(currentuser, bornes.get(borneSelected), reservation);
						Frais frais = new Frais(TypeFrais.contrat, reservation);
						M_Gestion.souscrireAbonnement(contrat, reservation, frais);
						currentuser.addContrat(contrat);
					} else {
						System.out.println("Pas de bornes disponibles");
					}

					break;
				case 6:
					if (currentuser.getTypeutil().equals(TypeUtilisateur.abonne)
							|| currentuser.getTypeutil().equals(TypeUtilisateur.exploitant)) {
						// M_Gestion.retournerContrats(currentuser).forEach(System.out::println);

						int cpt = 0;
						// List<Contrat> contrats = M_Gestion.retournerContrats(currentuser);
						List<Contrat> contrats = currentuser.getContrats();
						for (Contrat c : contrats) {
							System.out.println(cpt + " " + c.toString());
							cpt++;
						}
						if (contrats.size() > 0) {

							int contratSelected = -1;
							while (contratSelected < 0 || contratSelected > contrats.size() - 1) {
								System.out.println("Sélectionnez un des contrats disponibles");
								contratSelected = M_Planification.saisieInt();
							}
							M_Gestion.annulerAbonnement(contrats.get(contratSelected), currentuser);
							currentuser.removeContrat(contrats.get(contratSelected));
						}

					}
					break;
				case 7:
					if (currentuser.getTypeutil().equals(TypeUtilisateur.exploitant)) {
						M_Gestion.consulterProfils(currentuser).forEach(System.out::println);
					}
					break;

				case 8:
					if (currentuser.getTypeutil().equals(TypeUtilisateur.exploitant)) {
						System.out.println("Quel prix standard voulez-vous modifier ?");

						for (TypeFrais t : TypeFrais.values()) {
							System.out.println(t.toString() + " : " + t.getPrix());
							System.out.println("oui pour modifier");
							String modifier = in.readLine();
							if (modifier.equals("oui")) {
								double prix = 0.0;
								while (prix < 1.0) {
									System.out.println("Saisir le nouveau prix");
									prix = M_Planification.saisieDouble();
									t.modifierPrix(prix);
								}

							}
						}
					}
					break;

				case 9:
					quit = true;
					break;
				case 10:
					currentuser = null;
					break;
				case 11:
					System.out.println("Voici la liste des bornes indisponibles actuellement :");
					List<Borne> bornesindispos = M_Planification.listeBornesIndisponibles();
					for (i=0;i<bornesindispos.size();i++) {
						System.out.println("Identifiant : "+bornesindispos.get(i).getId());
					}
					break;
				default:
					System.out.println("Veuillez faire un choix valide");
					break;
				}
			}
		}
	}

}
