package classes_techniques;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Utilisateur {

	private int id;
	private String nom;
	private String prenom;
	private String adresse;
	private String mobile;
	private String mail;
	private TypeUtilisateur typeutil;
	private List<Vehicule> vehicules;
	private List<Reservation> reservations;
	private List<Contrat> contrats;
	private String resa;

	public Utilisateur(int id, String nom, String prenom, String adresse, String mobile, String mail, TypeUtilisateur typeutil) {
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.adresse=adresse;
		this.mobile=mobile;
		this.mail=mail;
		this.typeutil=typeutil;
		this.vehicules = new ArrayList<Vehicule>();
		this.reservations = new ArrayList<Reservation>();
		this.contrats = new ArrayList<Contrat>();
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public TypeUtilisateur getTypeutil() {
		return typeutil;
	}

	public void setTypeutil(TypeUtilisateur typeutil) {
		this.typeutil = typeutil;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}
	
	
	
	public List<Vehicule> getVehicules(){
		return new ArrayList<Vehicule>(this.vehicules);
	}
	
	public void setVehicules(List<Vehicule> vehicules) {
		this.vehicules = vehicules;
	}

	public void addVehicule(Vehicule v) {
		vehicules.add(v);
	}
	
	public List<Reservation> getReservations(){return new ArrayList<Reservation>(this.reservations);}

	public String voirReservations(){
		String resa = "Liste des réservations effectuées : \n";
		for (int i=0;i<this.reservations.size();i++){
			resa+="   * ["+reservations.get(i).getId()+"] Réservation le " + this.reservations.get(i).getDateDebut() + ", jusqu'au " + this.reservations.get(i).getDateFin() + "\n"
					+ "     Borne n°" + this.reservations.get(i).getBorne().getId() + "\n" ;
		}
		return resa;
	}

	public List<Integer> getIdResa(){
		ArrayList<Integer> listId = new ArrayList<Integer>();
		for (int i=0;i<this.reservations.size();i++){
			listId.add(reservations.get(i).getId());
		}
		return listId;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public void addReservation(Reservation r) {
		reservations.add(r);
	}
	
	public List<Contrat> getContrats(){
		return new ArrayList<Contrat>(this.contrats);
	}
	
	public void addContrat(Contrat c) {
		contrats.add(c);
	}
	
	public void removeContrat(Contrat c) {
		contrats.remove(c);
	}
	
	public void afficheVehicules() throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		String affichage="Voici la liste des plaques enregistrés :\n";
		for (int i=0;i<vehicules.size();i++) {
			affichage+="Plaque : "+vehicules.get(i).getPlaque()+"\n";
		}
		System.out.println(affichage);

		boolean quit = false;
		while(quit!=true) {
			System.out.println("Que voulez vous faire ? \n [1] Ajouter un véhicule \n [2] Retour accueil");
			String commande = in.readLine();
			switch(commande){
				case "1" :
					System.out.println("ajout !");

					break;
				case "2" :
					quit=true;
					break;
				default:
					System.out.println("Erreur, veuillez entrer une valeur proposée.");
			}
		}

	}

	public void setResa(String reservation) {
		this.resa=reservation;
	}

	public String getResa() {
		return this.resa;
	}
	
	public void setContrats(List<Contrat> contrats) {
		this.contrats = contrats;
	}

	public String toString() {
		return typeutil.toString() +  " : " + nom + " " + prenom + " " + mail + " " + adresse + " " + mobile;
	}
}
