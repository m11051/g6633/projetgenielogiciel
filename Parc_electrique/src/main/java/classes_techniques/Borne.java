package classes_techniques;

import java.util.ArrayList;
import java.util.List;

public class Borne {

	private int id;
	private EtatBorne etat;
	private Contrat contrat;
	private List<Reservation> reservations; //TODO Vérifier utilité
	
	public Borne(EtatBorne etat) {
		this.etat=etat;
		this.reservations = new ArrayList<Reservation>();
	}

	public Borne(int idBorne, String etat) {
		this.id=idBorne;
		for (EtatBorne etatSing:EtatBorne.values()) {
			this.etat=etatSing;
		}
	}

	public EtatBorne getEtat() {
		return etat;
	}

	public void setEtat(EtatBorne etat) {
		this.etat = etat;
	}

	public int getId() {
		return id;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}
	
	public List<Reservation> getReservations(){
		return new ArrayList<Reservation>(this.reservations);
	}
	
	public void addReservation(Reservation r) {
		reservations.add(r);
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String toString() {
		return "Borne " + id + "etat "+etat;
	}
	
}
