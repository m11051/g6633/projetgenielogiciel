package classes_techniques;

public class Frais {

	private int id;
	private TypeFrais type;
	private double montant;
	private Reservation reservation;
	
	public Frais(TypeFrais type, Reservation reservation) {
		this.type=type;
		this.montant=type.getPrix();
		this.reservation=reservation;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public int getId() {
		return id;
	}

	public TypeFrais getType() {
		return type;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	
}
