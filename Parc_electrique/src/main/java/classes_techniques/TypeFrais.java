package classes_techniques;

public enum TypeFrais {
	normal(10),depassement(12),penalite(6),contrat(20);
	
	private double prix;
	
	private TypeFrais(double prix) {
		this.prix=prix;
	}
	
	public void modifierPrix(double prix) {
		if(!this.equals(contrat)) //on ne peut pas modifier le prix des contrats
			this.prix=prix;
	}
	
	public double getPrix() {
		return prix;
	}
}
