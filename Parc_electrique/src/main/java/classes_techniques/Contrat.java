package classes_techniques;

public class Contrat {

	private int id;
	private Utilisateur utilisateur;
	private Borne borne;
	private Reservation reservation;
	
	public Contrat(Utilisateur utilisateur, Borne borne, Reservation reservation) {
		this.utilisateur=utilisateur;
		this.borne=borne;
		this.reservation=reservation;
	}

	public int getId() {
		return id;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public Borne getBorne() {
		return borne;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Reservation getReservation() {
		return reservation;
	}
	
	public String toString() {
		return "Contrat : " + " concernant " + utilisateur.toString() + " allant de " + reservation.getDateDebut() + " à " + reservation.getDateFin();
	}
}
