package classes_techniques;

import modeles.M_Gestion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static modeles.M_Gestion.typeClient;

public class Reservation {

	private int id;
	private LocalDateTime dateDebut;
	private LocalDateTime dateFin;
	private int prolongation;
	private Utilisateur utilisateur;
	private Borne borne;
	private List<Frais> frais;

	public Reservation(LocalDateTime dateDebut, LocalDateTime dateFin, Borne borne, Utilisateur utilisateur) {
		this.id=0;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.borne = borne;
		this.utilisateur = utilisateur;
	}

	public Reservation(String dateDebut, String dateFin, String idBorne, String idUtilisateur) {
		this.id=0;
		this.borne = M_Gestion.retournerBorne(Integer.parseInt(idBorne));
		this.utilisateur = M_Gestion.retournerUtilisateur(idUtilisateur);
	}


	public LocalDateTime getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDateTime dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDateTime getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDateTime dateFin) {
		this.dateFin = dateFin;
	}

	public int getProlongation() {
		return prolongation;
	}

	public void setProlongation(int prolongation) {
		this.prolongation = prolongation;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setIdutilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public int getId() {
		return id;
	}
	
	public List<Frais> getFrais(){
		return new ArrayList<Frais>(this.frais);
	}
	
	public void addFrais(Frais f) {
		frais.add(f);
	}

	public void setId(int id) {
		this.id = id;
	}

	public Borne getBorne() {
		return borne;
	}

	@Override
	public String toString() {
		return "Reservation{" +
				"dateDebut=" + dateDebut +
				", dateFin=" + dateFin +
				", prolongation=" + prolongation +
				", utilisateur=" + utilisateur +
				", frais=" + frais +
				'}';
	}

}
