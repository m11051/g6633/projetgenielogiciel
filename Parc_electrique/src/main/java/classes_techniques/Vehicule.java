package classes_techniques;

import java.util.ArrayList;
import java.util.List;

public class Vehicule {

	private int id;
	private String plaque;
	private List<Utilisateur> conducteurs;
	
	public Vehicule(String plaque) {
		this.id=id;
		this.plaque=plaque;
		conducteurs = new ArrayList<Utilisateur>();
	}

	public String getPlaque() {
		return plaque;
	}

	public void setPlaque(String plaque) {
		this.plaque = plaque;
	}

	public int getId() {
		return id;
	}
	
	public List<Utilisateur> getUtilisateurs(){
		return new ArrayList<Utilisateur>(this.conducteurs);
	}
	
	public void addConducteur(Utilisateur u) {
		conducteurs.add(u);
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
