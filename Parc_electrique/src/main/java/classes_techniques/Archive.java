package classes_techniques;

import java.time.LocalDateTime;

public class Archive {
	
	private int id;
	private Integer reservation;
	private LocalDateTime dateDebut;
	private LocalDateTime dateFin;
	//private Utilisateur utilisateur_reservation; //complexifie beaucoup la récupération
	private Integer frais;
	private TypeFrais type;
	private double montant;
	private Integer contrat;
	//private Utilisateur utilisateur_contrat;
	
	//public Archive(Integer contrat, Integer frais, String type, double montant, Utilisateur utilisateur_contrat) {
	public Archive(Integer contrat, Integer frais, String type, double montant) {
		this.contrat=contrat;
		this.frais=frais;
		this.type=TypeFrais.valueOf(type);
		this.montant=montant;
		//this.utilisateur_contrat=utilisateur_contrat;
	}
	
	//public Archive(Integer reservation, LocalDateTime dateDebut, LocalDateTime dateFin,Utilisateur utilisateur_reservation, Integer frais, TypeFrais type, double montant) {
	public Archive(Integer reservation, LocalDateTime dateDebut, LocalDateTime dateFin, Integer frais, String type, double montant) {
		this.reservation=reservation;
		this.dateDebut=dateDebut;
		this.dateFin=dateFin;
		this.frais=frais;
		this.type=TypeFrais.valueOf(type);
		this.montant=montant;
		//this.utilisateur_reservation=utilisateur_reservation;
	}
	

	public int getId() {
		return id;
	}

	public Integer getReservation() {
		return reservation;
	}

	public Integer getFrais() {
		return frais;
	}

	public Integer getContrat() {
		return contrat;
	}


	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDateTime dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDateTime getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDateTime dateFin) {
		this.dateFin = dateFin;
	}

	public TypeFrais getType() {
		return type;
	}

	public void setType(TypeFrais type) {
		this.type = type;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public void setReservation(Integer reservation) {
		this.reservation = reservation;
	}

	public void setFrais(Integer frais) {
		this.frais = frais;
	}

	public void setContrat(Integer contrat) {
		this.contrat = contrat;
	}
	
	
	
}
