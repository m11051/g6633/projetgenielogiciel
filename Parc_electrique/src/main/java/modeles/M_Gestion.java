package modeles;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import classes_techniques.Archive;
import classes_techniques.Borne;
import classes_techniques.Contrat;
import classes_techniques.EtatBorne;
import classes_techniques.Frais;
import classes_techniques.Reservation;
import classes_techniques.TypeFrais;
import classes_techniques.TypeUtilisateur;
import classes_techniques.Utilisateur;
import classes_techniques.Vehicule;
import modeles.M_Bdd;

public class M_Gestion {
	

	public static void main(String[] args) {
		
		
		Vehicule v = new Vehicule("AA-122-EE");
		Utilisateur lorrain = retournerUtilisateur("lorrain@gmail.com");
		System.out.println(lorrain.getId());
		retournerContrats(lorrain);
		
		
		
		
		/*Utilisateur lorrain = retournerUtilisateur("lorrain@gmail.com");
		Borne borne = new Borne(EtatBorne.disponible);
		borne.setId(1);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateDebut = LocalDateTime.parse("2022-05-03 00:00:00", formatter);
		LocalDateTime dateFin = LocalDateTime.parse("2022-05-04 00:00:00", formatter);
		Reservation reservation = new Reservation(dateDebut, dateFin, borne, lorrain);		
		Contrat abo = new Contrat(lorrain, borne, reservation);
		abo.setId(5);
		annulerAbonnement(abo, lorrain);*/
		//sysoM_Gestion.connexion("ernesto@gmail.com", "mdp");

	}
	
	/**
	* Retourne l'utilisateur inséré, lors de l'inscription, l'inscrit sera forcémnt de type client
	* Pas de test associé car si on teste la fonction, cela produit l'insertion en base de doonées
	* exemple d'utilisation :
	* M_Gestion.inscription("prenomtest1", "nomtest1", "adresses test1", "mdp test1", "06060606", "mail test1");
	*
	* @param  nom
	* @param  prenom
	* @param  adresse
	* @param  mdp
	* @param  mobile
	* @param  mail
	* @return utilisateur inséré
	* 
	*/
	public static Utilisateur inscription(String nom, String prenom, String adresse, String mdp, String mobile, String mail) {		
		String type = TypeUtilisateur.client.toString();
		String requete = "INSERT INTO Utilisateur(nom,prenom,mdp,adresse,mobile,mail,type) VALUES(?,?,?,?,?,?,?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.setString(1, nom);
			prepare.setString(2, prenom);
			prepare.setString(3, mdp);
			prepare.setString(4, adresse);
			prepare.setString(5, mobile);
			prepare.setString(6, mail);
			prepare.setString(7, type);
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace(); }
		String requeteSelection = "SELECT mail FROM utilisateur where mail='"+mail+"'";
		return retournerUtilisateur(M_Bdd.requeteSelection(requeteSelection,false).get(0).get("mail"));
	}
	
	/**
	* Retourne l'utilisateur qui veut se connecter
	* Renvoie null si connexion rejetée
	* exemple d'utilisation :
	* M_Gestion.connexion("ernesto@gmail.com", "mdp");
	*
	* @param  mail
	* @param  mdp
	* @return Utilisateur qui veut  se connecter
	* 
	*/
	public static Utilisateur connexion(String mail, String mdp) {
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		String requeteSelection = "SELECT type FROM utilisateur where mail='"+mail+"' and mdp='"+mdp+"'";
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		if(resultat==null) return null;
		return retournerUtilisateur(mail);
	}
	
	/**
	* Retourne type si de l'utilisateur
	* Renvoie null si utilisateur  n'existe pas
	* exemple d'utilisation :
	* M_Gestion.typeClient(1);
	*
	* @param  id
	* @return TypeUtilisateur
	* 
	*/
	public static TypeUtilisateur typeClient(int id) {
		String requeteSelection = "SELECT type FROM utilisateur where id='"+id+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		if(resultat == null) return null;
		return TypeUtilisateur.valueOf(resultat.get(0).get("type"));
	}
	
	/**
	* Retourne vrai si l'utilisateur est un simple client, update la mise à jour du statut de l'abonne en base
	* Renvoie faux si utilisateur pas client ou n'existe pas
	* pas de tests car modification base de données
	* exemple d'utilisation :
	* M_Gestion.devenirAbonne(1);
	*
	* @param  id
	* @return booléen 
	* 
	*/
	public static boolean devenirAbonne(int id, Contrat contrat, Reservation reservation, Frais frais) {
		if(typeClient(id).equals(TypeUtilisateur.client)) {
			Connection connection=null;
			try { connection=M_Bdd.connexion(); } 
			catch (ClassNotFoundException e) { e.printStackTrace(); }
			String type = TypeUtilisateur.abonne.toString();
			String requete = "UPDATE Utilisateur SET type=? WHERE id=?";
			try {
				PreparedStatement prepare = connection.prepareStatement(requete);
				prepare.setString(1, type);
				prepare.setInt(2, id);
				prepare.executeUpdate();
				souscrireAbonnement(contrat,reservation,frais);
				return true;
			} catch (SQLException e) { e.printStackTrace(); }
		}
		return false;
	}
	
	
	/**
	* Retourne vrai si l'utilisateur est un abonne et que son contrat et toutes ses dépendances ont été insérées en base
	* Renvoie faux sinon
	* Fonction connexe avec ajoutContrat,ajoutReservation et ajoutFrais ci-dessous
	* pas de tests car modification base de données
	* exemple utilisation :
	* 	Utilisateur lorrain = retournerUtilisateur("lorrain@gmail.com");
		Borne borne = new Borne(EtatBorne.disponible);
		borne.setId(1);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateDebut = LocalDateTime.parse("2022-05-03 00:00:00", formatter);
		LocalDateTime dateFin = LocalDateTime.parse("2022-05-04 00:00:00", formatter);
		Reservation reservation = new Reservation(dateDebut, dateFin, borne, lorrain);
		Contrat contrat = new Contrat(lorrain, borne,reservation);
        SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Frais frais = new Frais(TypeFrais.contrat, reservation);
		souscrireAbonnement(contrat,reservation,frais);
	*
	* @param  id
	* @return booléen 
	* 
	*/
	public static boolean souscrireAbonnement(Contrat contrat, Reservation reservation, Frais frais) {
		//TODO modifier statut utilisateur dans collecction à charger
		Utilisateur user = contrat.getUtilisateur();
		System.out.println(typeClient(contrat.getUtilisateur().getId()));
		if(contrat.getUtilisateur().getTypeutil().equals(TypeUtilisateur.client)) {
			user.setTypeutil(TypeUtilisateur.abonne);
			user = modifierUtilisateur(user);
		}
		//ajout du contrat
		if(user.getTypeutil().equals(TypeUtilisateur.abonne)) {
			
			Reservation res = ajoutReservation(reservation);
			contrat.setReservation(res);
			ajoutContrat(contrat);
			frais.setReservation(res);
			ajoutFrais(frais);
			return true;
		}
		return false;
	}

	public static Contrat ajoutContrat(Contrat contrat) {
		String requete = "INSERT INTO Contrat(idBorne,idUtilisateur,idReservation) VALUES(?,?,?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.setInt(1, contrat.getBorne().getId());
			prepare.setInt(2, contrat.getUtilisateur().getId());
			prepare.setInt(3, contrat.getReservation().getId());
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();return null; }
		String requeteSelection = "SELECT Max(id) as maxid FROM Contrat";
		contrat.setId(Integer.parseInt(M_Bdd.requeteSelection(requeteSelection,false).get(0).get("maxid")));
		return contrat;
	}
	
	public static Reservation ajoutReservation(Reservation reservation) {
		String requete = "INSERT INTO Reservation(dateDebut,dateFin,prolongation,idUtilisateur,idBorne) VALUES(?,?,?,?,?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.setObject(1, reservation.getDateDebut());
			prepare.setObject(2, reservation.getDateFin());
			prepare.setInt(3, reservation.getProlongation()+1);
			prepare.setInt(4, reservation.getUtilisateur().getId());
			prepare.setInt(5, reservation.getBorne().getId());
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();return null; }
		String requeteSelection = "SELECT Max(idresa) as maxid FROM Reservation";
		reservation.setId(Integer.parseInt(M_Bdd.requeteSelection(requeteSelection,false).get(0).get("maxid")));
		return reservation;
	}
	
	public static Frais ajoutFrais(Frais frais) {
		String requete = "INSERT INTO Frais(montant,idRes,typeFrais) VALUES(?,?,?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.setDouble(1, frais.getMontant());
			prepare.setDouble(2, frais.getReservation().getId());
			prepare.setString(3, frais.getType().toString());
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();return null; }
		String requeteSelection = "SELECT Max(idfr) as maxid FROM Frais";
		frais.setId(Integer.parseInt(M_Bdd.requeteSelection(requeteSelection,false).get(0).get("maxid")));
		return frais;
	}

	/**
	 * Retourne utilisateur concerné
	 * Renvoie null si mail utilisateur n'existe pas
	 * pas de tests car utilisation base de données
	 * exemple d'utilisation :
	 * M_Gestion.retournerUtilisateur("ernesto@gmail.com")
	 *
	 * @param  idBorne
	 * @return Borne
	 *
	 */
	public static Borne retournerBorne(int idBorne) {
		String requeteSelection = "SELECT * FROM borne where id='"+idBorne+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); }
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		if(resultat != null) return new Borne(idBorne, resultat.get(0).get("etat"));
		return null;
	}


	/**
	* Retourne utilisateur concerné
	* Renvoie null si mail utilisateur n'existe pas
	* pas de tests car utilisation base de données
	* exemple d'utilisation :
	* M_Gestion.retournerUtilisateur("ernesto@gmail.com")
	*
	* @param  mail
	* @return Utilisateur
	* 
	*/
	public static Utilisateur retournerUtilisateur(String mail) {
		String requeteSelection = "SELECT id,nom,prenom,adresse,mail,mobile,type FROM utilisateur where mail='"+mail+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		if(resultat != null) return new Utilisateur(Integer.parseInt(resultat.get(0).get("id")), resultat.get(0).get("nom"), resultat.get(0).get("prenom"), resultat.get(0).get("adresse"), resultat.get(0).get("mobile"), resultat.get(0).get("mail"), TypeUtilisateur.valueOf(resultat.get(0).get("type")));
		return null;
	}

	/**
	 * Retourne utilisateur concerné
	 * Renvoie null si mail utilisateur n'existe pas
	 * pas de tests car utilisation base de données
	 * exemple d'utilisation :
	 * M_Gestion.retournerUtilisateur(1)
	 *
	 * @param  idUtilisateur
	 * @return Utilisateur
	 */
	public static Utilisateur retournerUtilisateur(int idUtilisateur) {
		String requeteSelection = "SELECT nom,prenom,adresse,mail,mobile,type FROM utilisateur where id='"+idUtilisateur+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); }
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		if(resultat != null) return new Utilisateur(idUtilisateur, resultat.get(0).get("nom"), resultat.get(0).get("prenom"), resultat.get(0).get("adresse"), resultat.get(0).get("mobile"), resultat.get(0).get("mail"), TypeUtilisateur.valueOf(resultat.get(0).get("type")));
		return null;
	}

	/**
	* Retourne utilisateur concerné et met à jour ses données en base
	* pas de tests car utilisation base de données
	* Cette version n'est pas sécuriése contre les injections sql mais je n'arrive pas à la mettre en place (code en commentaire)
	* exemple d'utilisation :
	* 
	* Utilisateur lorrain = retournerUtilisateur("lorrain@gmail.com");
	*	lorrain.setAdresse("Norroy");
	*	modifierUtilisateur(lorrain);
	*
	* @param  utilisateur
	* @return Utilisateur
	* 
	*/
	public static Utilisateur modifierUtilisateur(Utilisateur utilisateur) {
		//String requete = "UPDATE Utilisateur SET nom=n, prenom=p, adresse=a, mobile=m WHERE id=2 VALUES(?,?,?,?)";
		String requete = "UPDATE Utilisateur SET nom='"+utilisateur.getNom()+
				"',prenom='"+utilisateur.getPrenom()+
				"',adresse='"+utilisateur.getAdresse()+
				"',type='"+utilisateur.getTypeutil().toString()+
				"',mobile='"+utilisateur.getMobile()+
				"' WHERE id="+utilisateur.getId();
		
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			/*prepare.setString(1, utilisateur.getNom());
			prepare.setString(2, utilisateur.getPrenom());
			prepare.setString(3, utilisateur.getAdresse());
			prepare.setString(4, utilisateur.getMobile());
			prepare.setInt(5, utilisateur.getId());*/
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();return null; }
		return utilisateur;
	}
	
	/**
	* Retourne la liste des profils utilisateurs qui ne sont pas des exploitants
	* pas de tests car utilisation base de données
	* Ne peut-être utilisée que par un exploitant
	* exemple d'utilisation :
	* 
	* Utilisateur ernesto = retournerUtilisateur("ernesto@gmail.com");
	*	consulterProfils(ernesto);
	*
	* @param  utilisateur
	* @return List<Map<String, String>>
	* 
	*/
	public static List<Utilisateur> consulterProfils(Utilisateur utilisateur) {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		List<Map<String, String>> resultat =null;
		if(utilisateur.getTypeutil().equals(TypeUtilisateur.exploitant)) {
			String requeteSelection = "SELECT id,nom,prenom,adresse,mail,mobile,type FROM utilisateur where type IN('"+TypeUtilisateur.client.toString()+"','"+TypeUtilisateur.abonne.toString()+"')";
			Connection connection=null;
			try { connection=M_Bdd.connexion(); } 
			catch (ClassNotFoundException e) { e.printStackTrace(); }
			resultat = M_Bdd.requeteSelection(requeteSelection,false);
			
			for (Map<String, String> m : resultat) {
				Utilisateur u=new Utilisateur(Integer.parseInt(m.get("id")),m.get("nom"), m.get("prenom"), m.get("adresse"), m.get("mail"), m.get("mobile"), TypeUtilisateur.valueOf(m.get("type")));
				utilisateurs.add(u);
			}
		}
		return utilisateurs;
	}


	public static List<Reservation> retournerReservation(Utilisateur utilisateur){
		List<Map<String, String>> resultat = null;
		String requeteSelection = "SELECT * FROM reservation WHERE idUtilisateur="+utilisateur.getId();
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		List<Reservation> reservations=new ArrayList<Reservation>();
		if (resultat == null) {
			return reservations;
		}
		for (Map<String, String> m : resultat) {
			Reservation resa=new Reservation(m.get("dateDebut"), m.get("dateFin"), m.get("idBorne"), m.get("idUtilisateur"));
			reservations.add(resa);
		}
		return reservations;
	}


	public static List<Vehicule> retournerVehicules(Utilisateur utilisateur) {
		List<Map<String, String>> resultat = null;
		String requeteSelection = "SELECT v.id, plaque FROM utilisateur u INNER JOIN vehicule_utilisateur vu ON vu.idUtilisateur=u.id INNER JOIN vehicule v ON v.id=vu.idVehicule WHERE u.id="+utilisateur.getId();
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		List<Vehicule> vehicules=new ArrayList<Vehicule>();
		if (resultat == null) {
			return vehicules;
		}
		for (Map<String, String> m : resultat) {
			Vehicule v=new Vehicule(m.get("plaque"));
			v.setId(Integer.parseInt(m.get("id")));
			vehicules.add(v);
		}
		return vehicules;
	}
	
	public static List<Reservation> retournerReservationsAvecBorne(Borne borne, Utilisateur utilisateur) {
		//SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
  		List<Map<String, String>> resultat = null;
		String requeteSelection = "SELECT dateDebut, dateFin, prolongation, idUtilisateur FROM utilisateur u INNER JOIN reservation r ON r.idUtilisateur=u.id WHERE u.id="+utilisateur.getId();
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		List<Reservation> reservations=new ArrayList<Reservation>();
		if (resultat == null) {
			return reservations;
		}
		for (Map<String, String> m : resultat) {
			Reservation r=null;
			//TODO A tester
			LocalDateTime dateDebut = LocalDateTime.parse(m.get("dateDebut"), formatter);
			LocalDateTime dateFin = LocalDateTime.parse(m.get("dateFin"), formatter);
			r = new Reservation(dateDebut,dateFin,borne,utilisateur);
			reservations.add(r);
		}
		return reservations;
	}
	
	public static List<Archive> retournerArchives(){
		List<Map<String, String>> resultat = null;
		//String requeteSelection = "SELECT r.id as idres ,dateDebut, dateFin,f.id as idFrais,f.typeFrais as type,f.montant as montant, c.id as idcontrat FROM reservation r"
		//		+ " INNER JOIN frais f on f.idRes=r.id"
		//		+ " LEFT OUTER JOIN contrat c ON c.idReservation=r.id";
		String requeteSelection = "select * from archives";
		System.out.println(requeteSelection);
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		List<Archive> archives=new ArrayList<Archive>();
		if (resultat == null) {
			return archives;
		}
		for (Map<String, String> m : resultat) {
			Archive a=null;
			if(m.get("idContrat")!=null) 
				//Utilisateur u = new Utilisateur(Integer.parseInt(m.get("c.id")), requeteSelection, requeteSelection, requeteSelection, requeteSelection, requeteSelection, null)
				a = new Archive(Integer.parseInt(m.get("idContrat")), Integer.parseInt(m.get("idFrais")), m.get("typeFrais"), Double.parseDouble(m.get("montant")));
			else {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime dateDebut = LocalDateTime.parse(m.get("dateDebut"), formatter);
				LocalDateTime dateFin = LocalDateTime.parse(m.get("dateFin"), formatter);
			
				a = new Archive(Integer.parseInt(m.get("idRes")), dateDebut, dateFin, Integer.parseInt(m.get("idFrais")), m.get("typeFrais"), Double.parseDouble(m.get("montant")));
			}
			archives.add(a);
		}
		return archives;
	}
	
	public static List<Contrat> retournerContrats(Utilisateur u){
		List<Map<String, String>> resultat = null;
		//String requeteSelection = "SELECT r.id as idres ,dateDebut, dateFin,f.id as idFrais,f.typeFrais as type,f.montant as montant, c.id as idcontrat FROM reservation r"
		//		+ " INNER JOIN frais f on f.idRes=r.id"
		//		+ " LEFT OUTER JOIN contrat c ON c.idReservation=r.id";
		String requeteSelection = "select c.id as test, c.idBorne, nom,prenom,adresse,mobile,mail,type,etat,idReservation,dateDebut,dateFin from contrat c inner join Borne on c.idBorne=Borne.id inner join Reservation r on idresa=c.idReservation inner join utilisateur u on u.id=c.idUtilisateur where c.idUtilisateur="+u.getId();
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		List<Contrat> contrats=new ArrayList<Contrat>();
		if (resultat == null) {
			return contrats;
		}
		for (Map<String, String> m : resultat) {
			Borne b = new Borne(EtatBorne.valueOf(m.get("etat")));
			b.setId(Integer.parseInt(m.get("id")));
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateDebut = LocalDateTime.parse(m.get("dateDebut"), formatter);
			LocalDateTime dateFin = LocalDateTime.parse(m.get("dateFin"), formatter);
			Reservation r = new Reservation(dateDebut, dateFin, b, u);
			r.setId(Integer.parseInt(m.get("idReservation")));
			Contrat c=new Contrat(u, b, r);
			c.setId(Integer.parseInt(m.get("id")));
			
			contrats.add(c);
		}
		return contrats;
	}
	
	/**
	* Retourne l'utilisateur qui peut avoir été mis à jour créée
	* pas de tests car utilisation base de données
	* exemple d'utilisation :
	* 
	Utilisateur lorrain = retournerUtilisateur("lorrain@gmail.com");
		Borne borne = new Borne(EtatBorne.disponible);
		borne.setId(1);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateDebut = LocalDateTime.parse("2022-05-03 00:00:00", formatter);
		LocalDateTime dateFin = LocalDateTime.parse("2022-05-04 00:00:00", formatter);
		Reservation reservation = new Reservation(dateDebut, dateFin, borne, lorrain);		
		Contrat abo = new Contrat(lorrain, borne, reservation);
		abo.setId(5);
		annulerAbonnement(abo, lorrain);
	*
	* @param  contrat,utilisateur
	* @return utilisateur
	* 
	*/
	
	public static Utilisateur annulerAbonnement(Contrat contrat,Utilisateur utilisateur) {
		
		//sélection de tous les frais associés au contrat nok
		//ajout de otus les frias du contrat dans archive bdd ok
		//suppression de tous les frais ok
		//suppression du contrat ok
		//si utilisateur plus de contrat alors 
		//suppression contrat dans objet liste : menu
		//suppression archive dans objet liste : menu
		List<Archive> retour = new ArrayList<Archive>();
		if(utilisateur.getTypeutil().equals(TypeUtilisateur.abonne)) {
			
			List<Frais> frais = retournerFraisContrat(contrat,utilisateur);
			for(Frais f : frais) {
				Archive archive = new Archive(contrat.getId(), f.getId(), f.getType().toString(), f.getMontant());
				ajoutArchiveContrat(archive);
				supprimerFraisContrat(f);
				retour.add(archive);
			}
			supprimerContrat(contrat);
			if(!nbAbonnementValide(utilisateur)) {
				utilisateur.setTypeutil(TypeUtilisateur.client);
				modifierUtilisateur(utilisateur);
			}
		}
		return utilisateur;
	}
	
	//TODO pas testée
	public static Archive ajoutArchiveContrat(Archive archive) {
		//public Archive(Integer reservation, LocalDateTime dateDebut, LocalDateTime dateFin,Utilisateur utilisateur_reservation, Integer frais, TypeFrais type, double montant) {
		String requete = "INSERT INTO Archives(idRes,dateDebut,dateFin,idFrais,typeFrais,montant,idContrat) VALUES(?,?,?,?,?,?,?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			if(archive.getReservation()!=null) prepare.setInt(1, archive.getReservation());
			else prepare.setObject(1,null);
			if(archive.getDateDebut()!=null) prepare.setObject(2, archive.getDateDebut());
			else prepare.setObject(2,null);
			if(archive.getReservation()!=null) prepare.setObject(3, archive.getDateDebut());
			else prepare.setObject(3,null);
			prepare.setInt(4, archive.getFrais());
			prepare.setString(5, archive.getType().toString());
			prepare.setDouble(6, archive.getMontant());
			if(archive.getContrat()!=null) prepare.setObject(7, archive.getContrat());
			else prepare.setObject(7,null);
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();return null; }
		String requeteSelection = "SELECT Max(id) as maxid FROM Archives";
		archive.setId(Integer.parseInt(M_Bdd.requeteSelection(requeteSelection,false).get(0).get("maxid")));
		return archive;
	}
	
	//TODO pas testée
	public static void supprimerContrat(Contrat contrat) {
		
		String requete = "DELETE FROM Contrat WHERE id="+contrat.getId();
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.executeUpdate(requete);
		} catch (SQLException e) { e.printStackTrace(); }
	}
	

	//TODO pas testée
	public static void supprimerFraisContrat(Frais frais) {
		String requete = "DELETE FROM Frais WHERE idfr="+frais.getId();
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			//prepare.setInt(1, frais.getId());
			prepare.executeUpdate(requete);
		} catch (SQLException e) { e.printStackTrace(); }
	}
		
	//TODO pas testée
	public static List<Frais> retournerFraisContrat(Contrat contrat, Utilisateur utilisateur) {
		String requeteSelection = "SELECT idresa,idfr,dateDebut,dateFin,prolongation,r.idBorne as idborne,typeFrais,montant,etat FROM Contrat c inner join reservation r on c.idReservation=idresa inner join frais f on f.idRes=idresa  inner join Borne b on b.id=r.idBorne where c.id='"+contrat.getId()+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		List<Frais> frais=new ArrayList<Frais>();
		if (resultat == null) {
			return frais;
		}
		for (Map<String, String> m : resultat) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateDebut = LocalDateTime.parse(m.get("dateDebut"), formatter);
			LocalDateTime dateFin = LocalDateTime.parse(m.get("dateFin"), formatter);
			Borne borne = new Borne(EtatBorne.valueOf(m.get("etat")));
			Reservation r = new Reservation(dateDebut, dateFin, borne, utilisateur);
			r.setId(Integer.parseInt(m.get("idresa")));

			Frais f = new Frais(TypeFrais.valueOf(m.get("typeFrais")), r);
			f.setId(Integer.parseInt(m.get("idfr")));
			frais.add(f);
		}
		return frais;
	}
	
	//TODO pas testée
	public static boolean nbAbonnementValide(Utilisateur utilisateur) {
		String requeteSelection = "SELECT count(*) as c from Contrat where idUtilisateur='"+utilisateur.getId()+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		if(M_Bdd.requeteSelection(requeteSelection,false).get(0) == null || M_Bdd.requeteSelection(requeteSelection,false).get(0).get("c").equals("0"))
			return false;
		return true;
	}
		
	//TODO check si dans sa liste de vehicule dans le menu
	public static Vehicule ajoutPlaque(Utilisateur utilisateur, Vehicule vehicule) {
		Vehicule retour = null;
		if(!M_Reservation.verifImmat(vehicule.getPlaque())) return retour;
		if(M_Planification.RetournerProprietaireVehicule(vehicule.getPlaque())!=null) {
			//insertion dans vehicule_utilisateur
			vehicule = getVehicule(vehicule.getPlaque());
			inserer_vehicule_utilisateur(utilisateur,vehicule);
		}else {
			//insertion dans vehicule
			//insertion dans vehicule_utilisateur
			retour = inserer_vehicule(vehicule);
			inserer_vehicule_utilisateur(utilisateur,vehicule);
		}
		return vehicule;
	}

	private static Vehicule getVehicule(String plaque) {
		Vehicule vehicule = null;
		String requeteSelection = "SELECT * FROM vehicule where plaque='"+plaque+"'";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); }
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection,false);
		if(resultat != null) {
			vehicule = new Vehicule(plaque);
			vehicule.setId(Integer.parseInt(resultat.get(0).get("id")));
		}
		return vehicule;
	}

	private static Vehicule inserer_vehicule(Vehicule vehicule) {
		String requete = "INSERT INTO Vehicule(plaque) VALUES(?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.setString(1, vehicule.getPlaque());
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();return null; }
		String requeteSelection = "SELECT Max(id) as maxid FROM Vehicule";
		vehicule.setId(Integer.parseInt(M_Bdd.requeteSelection(requeteSelection,false).get(0).get("maxid")));
		return vehicule;
	}

	private static void inserer_vehicule_utilisateur(Utilisateur utilisateur, Vehicule vehicule) {
		String requete = "INSERT INTO Vehicule_utilisateur(idVehicule,idUtilisateur) VALUES(?,?)";
		Connection connection=null;
		try { connection=M_Bdd.connexion(); } 
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.setInt(1, vehicule.getId());
			prepare.setInt(2, utilisateur.getId());
			prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace(); }
		
	}
	
	
}
