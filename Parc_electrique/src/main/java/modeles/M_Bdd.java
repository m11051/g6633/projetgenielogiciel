package modeles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.cj.xdevapi.Result;

public class M_Bdd {
	
	private static String ip="localhost";
	private static String bdd="parc_electrique";
	private static String port=null;
	private static String user="root";
	private static String mdp="";
	
	private static Connection connexion;

	public static void main (String[] args) throws Exception {
		M_Bdd.connexion();
		List<Map<String, String>> test = requeteSelection("SELECT * FROM utilisateur",false);
		System.out.println(test);
		System.out.println(test.get(1).get("nom"));
	}
	
	/**
	* Retourne un objet connexion 
	* Ne reconnecte pas si la connexion est d�j� existante
	*
	* @return      La connexion qui va servir pour effectuer les diff�rentes requ�te
	* @throws ClassNotFoundException dans le cas ou le jar n'est pas précisé dans le pom
	*/
	public static Connection connexion() throws ClassNotFoundException {
		if(M_Bdd.connexion==null) {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = null;
			try {
				conn = DriverManager.getConnection("jdbc:mysql://"+ip+"/"+bdd,user,mdp);
			} catch (SQLException ex) {
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
			}
			M_Bdd.connexion=conn;
		}
		return M_Bdd.connexion;
	}
	
	/**
	* Retourne une liste de map repr�sentant les donn�es s�lectionn�es
	* exemple d'utilisation :
	* List<Map<String, String>> utilisateurs = requeteSelection("SELECT * FROM utilisateur",false); 
	* System.out.println(utilisateurs); //affichage tous les utilisateurs
	* System.out.println(utilisateurs.get(1).get("nom")); //affichage un utilisateur en particulier
	*
	* @param  query  an absolute URL giving the base location of the image
	* @param  affichage Vrai pour afficher les donn�es uniquement, faux pour produire une liste ds donn�es s�lectionn�es
	* @return      la liste de map associ�e
	* 
	*/
	public static List<Map<String, String>> requeteSelection(String query, boolean affichage) {
		//pas de param�tres par d�faut en java
		ResultSet results = null;
		List<Map<String, String>> resultats = new ArrayList<Map<String,String>>();
		try {
			Statement stmt = M_Bdd.connexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 
                    ResultSet.CONCUR_UPDATABLE);
			results = stmt.executeQuery(query);
			ResultSetMetaData rsmd = results.getMetaData();
			
			if(affichage) {
				while(results.next()){//only runs when there are rows in the resultset
					for(int i=1;i<=rsmd.getColumnCount();i++)  System.out.print(rsmd.getColumnName(i)+ " :" +results.getString(i) + " ");
					System.out.println();
				}
			}else {
				while(results.next()){
					Map<String,String> row = new HashMap<String,String>();
					for(int i=1;i<=rsmd.getColumnCount();i++)  row.put(rsmd.getColumnName(i), results.getString(i));
					resultats.add(row);
				}
			}
		}
		catch(Exception e){ System.out.println(e.getMessage()); }
		if(resultats.size()==0) return null;
		return resultats;
	}
	
	/**
	* Retourne un booleen pour savoir si l'insertion s'est bien passée
	* exemple d'utilisation :
	* List<Map<String, String>> utilisateurs = requeteSelection("SELECT * FROM utilisateur",false); 
	* System.out.println(utilisateurs); //affichage tous les utilisateurs
	* System.out.println(utilisateurs.get(1).get("nom")); //affichage un utilisateur en particulier
	*
	* @param  query  an absolute URL giving the base location of the image
	* @param  affichage Vrai pour afficher les donn�es uniquement, faux pour produire une liste ds donn�es s�lectionn�es
	* @return      la liste de map associ�e
	 * @throws ClassNotFoundException dans le cas ou le jar n'est pas précisé dans le pom
	*/
	public static boolean requeteManipulation(String query) throws ClassNotFoundException {	
		ResultSet results = null;
		try {
			Statement stmt = M_Bdd.connexion.createStatement();
			results = stmt.executeQuery(query);
		}
		catch(Exception e){ System.out.println(e.getMessage()); }
		return true;
	}
}