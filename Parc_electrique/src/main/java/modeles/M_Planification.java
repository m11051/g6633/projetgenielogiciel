package modeles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import com.google.protobuf.TextFormat.ParseException;

import classes_techniques.Borne;
import classes_techniques.EtatBorne;
import classes_techniques.Reservation;
import classes_techniques.TypeUtilisateur;
import classes_techniques.Utilisateur;

public class M_Planification {

	public static void main(String[] args) {
		 SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	     LocalDateTime datedebut=null;
	     LocalDateTime datefin=null;
	     datedebut = LocalDateTime.parse("12/05/2022 05:35:00");
		 datefin = LocalDateTime.parse("14/05/2022 05:35:00");
			
		List<Borne> bornes = recupererBornesDispos(datedebut,datefin);
		System.out.println(bornes.get(0).getId());
	}

	/**
	* Retourne la liste des bornes disponibles entre deux dates 
	* pas de tests car utilisation base de données
	* 
	* exemple d'utilisation :
	* 
	*  SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	     Date datedebut=null;
	     Date datefin=null;
	     try {
	    	 datedebut = dateParser.parse("12/05/2022 05:35:00");
			 datefin = dateParser.parse("14/05/2022 05:35:00");
		} catch (java.text.ParseException e) { e.printStackTrace(); }
			
		List<Borne> bornes = recupererBornesDispos(datedebut,datefin);
		System.out.println(bornes.get(0).getId());
	*
	* @param Date : date de début
	* @param Date : date de fin
	* @return List<Borne>
	* 
	*/
	public static List<Borne> recupererBornesDispos(LocalDateTime debut, LocalDateTime fin) {
		List<Map<String, String>> resultat =null;
		String requeteSelection = 
		"SELECT b.id, b.etat FROM borne b WHERE "
		+ "b.id not in  "
		+ "(select b.id FROM borne b inner JOIN reservation r  ON r.idborne=b.id WHERE "
		+ "(dateDebut BETWEEN '"+debut+"' and '"+fin+"')"
		+ " or (dateFin BETWEEN '"+debut+"' and '"+fin+"'))";
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		resultat = M_Bdd.requeteSelection(requeteSelection, false);

		List<Borne> bornes = new ArrayList<Borne>();
		if (resultat == null)
			return bornes;
		for (Map<String, String> m : resultat) {
			Borne b = new Borne(EtatBorne.valueOf(m.get("etat")));
			b.setId(Integer.parseInt(m.get("id")));
			bornes.add(b);
		}
		return bornes;
	}

	public static Utilisateur RetournerProprietaireVehicule(String plaque) {
		String requeteSelection = "SELECT u.id,nom,prenom,adresse,mail,mobile,type FROM utilisateur u INNER JOIN vehicule_utilisateur vu ON vu.idUtilisateur=u.id INNER JOIN vehicule v ON v.id=vu.idVehicule where plaque='"
				+ plaque + "'";
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		List<Map<String, String>> resultat = M_Bdd.requeteSelection(requeteSelection, false);
		if (resultat != null)
			return new Utilisateur(Integer.parseInt(resultat.get(0).get("id")), resultat.get(0).get("nom"),
					resultat.get(0).get("prenom"), resultat.get(0).get("adresse"), resultat.get(0).get("mobile"),
					resultat.get(0).get("mail"), TypeUtilisateur.valueOf(resultat.get(0).get("type")));
		return null;
	}
	
	public static int saisieInt() {
		Scanner sc=new Scanner(System.in);
		int saisie=-1;
		try {
			saisie=sc.nextInt();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saisie;
	}
	
	public static void reservationSurBorne(int duree,Borne borne,Utilisateur user) {
		Reservation r=new Reservation(LocalDateTime.now(),LocalDateTime.now().plusHours(duree),borne,user);
		r=M_Gestion.ajoutReservation(r);
}

	public static double saisieDouble() {
		Scanner sc=new Scanner(System.in);
		double saisie=-1;
		try {
			saisie=sc.nextDouble();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saisie;
	}

	public static List<Borne> listeBornesIndisponibles() {
		List<Map<String, String>> resultat =null;
		String requeteSelection = 
		"SELECT b.id, b.etat FROM borne b  WHERE etat='indisponible'";
		//System.out.println(requeteSelection);
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		resultat = M_Bdd.requeteSelection(requeteSelection, false);

		List<Borne> bornes = new ArrayList<Borne>();
		if (resultat == null)
			return bornes;
		for (Map<String, String> m : resultat) {
			Borne b = new Borne(EtatBorne.valueOf(m.get("etat")));
			b.setId(Integer.parseInt(m.get("id")));
			bornes.add(b);
		}
		return bornes;
	}
	
	public static Borne retournerBorneReservation(int idres) {
		List<Map<String, String>> resultat =null;
		String requeteSelection = "SELECT b.id FROM borne b INNER JOIN reservation r ON r.idBorne=b.id WHERE r.idresa="+idres;
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		Borne b=new Borne(EtatBorne.valueOf(resultat.get(0).get("etat")));
		b.setId(Integer.parseInt(resultat.get(0).get("id")));
		return b;
	}
	
	public static void prolongerDureeReservation(int idres, int minutes) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		List<Map<String, String>> resultat =null;
		String select = "SELECT dateFin FROM reservation WHERE idresa="+idres;
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		resultat=M_Bdd.requeteSelection(select, false);
		LocalDateTime dateFin=(LocalDateTime) LocalDateTime.parse(resultat.get(0).get("dateFin"),formatter).plusMinutes(minutes);
		String requete = " UPDATE reservation SET dateFin='"+dateFin
				+ "' WHERE idresa="+idres;
		try {
		PreparedStatement prepare = connection.prepareStatement(requete);
		prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();}
		
	}
	
	public static void depart(Reservation r) {
		String requete = " UPDATE borne SET etat='disponible' WHERE id="+r.getId();
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.executeUpdate();
			} catch (SQLException e) { e.printStackTrace();}
		
	}
	
	public static boolean arriveALHeure(Reservation resa) {
		if (LocalDateTime.now().isAfter(resa.getDateDebut()) && LocalDateTime.now().isBefore(resa.getDateDebut().plusMinutes(5))) {
			return true;
		}
		else return false;
	}
	
	public static boolean retardDansPA(Reservation resa) {
		if (LocalDateTime.now().isAfter(resa.getDateDebut().plusMinutes(5)) && LocalDateTime.now().isBefore(resa.getDateDebut().plusMinutes(15))) {
			return true;
		}
		else return false;
	}
	
	public static boolean retardSuperieurPA(Reservation resa) {
		if (LocalDateTime.now().isAfter(resa.getDateDebut().plusMinutes(15))) {
			return true;
		}
		else return false;
	}
	
	public static boolean verifBorneDispoPourProlong(Borne b,Reservation r,int duree) {
		List<Borne> bornesdispos=new ArrayList<Borne>();
		bornesdispos=recupererBornesDispos(r.getDateFin().plusSeconds(1),r.getDateFin().plusMinutes(duree));
		for (int i=0;i<bornesdispos.size();i++) {
			if (bornesdispos.get(i).equals(b)) {
				return true;
			}
		}
		return false;
	}
	
	public static Reservation getIdReservationActuelle(Utilisateur user) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		List<Map<String, String>> resultat =null;
		String requeteSelection ="SELECT * FROM reservation r INNER JOIN utilisateur u ON r.idUtilisateur=u.id"
				+ " WHERE (dateDebut < '"+LocalDateTime.now()+"')"
				+ " AND (dateFin > '"+LocalDateTime.now()+"')"
				+ " AND r.idUtilisateur="+user.getId();
		System.out.println(requeteSelection);
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		resultat = M_Bdd.requeteSelection(requeteSelection, false);
		Reservation r=new Reservation(LocalDateTime.parse(resultat.get(0).get("dateDebut"),formatter),LocalDateTime.parse(resultat.get(0).get("dateFin"),formatter),M_Gestion.retournerBorne(Integer.parseInt(resultat.get(0).get("idBorne"))),user);
		r.setId(Integer.parseInt(resultat.get(0).get("idresa")));
		return r;
	}
	
	public static void updateBorneResa(Reservation r, Borne b, LocalDateTime newDateFin) {
		String requete = " UPDATE reservation SET idBorne="+ b.getId()
				+ " WHERE idresa="+r.getId();
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
		PreparedStatement prepare = connection.prepareStatement(requete);
		prepare.executeUpdate();
		} catch (SQLException e) { e.printStackTrace();}
	}
	
	public static void bornePasseIndispo(Borne b) {
		String requete = " UPDATE borne SET etat='indisponible' WHERE id="+b.getId();
		Connection connection = null;
		try {
			connection = M_Bdd.connexion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			PreparedStatement prepare = connection.prepareStatement(requete);
			prepare.executeUpdate();
			} catch (SQLException e) { e.printStackTrace();}
		
	}
	
	
}
