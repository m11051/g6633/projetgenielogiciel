package modeles;

import classes_techniques.Borne;
import classes_techniques.Contrat;
import classes_techniques.TypeUtilisateur;
import classes_techniques.Utilisateur;
//import jdk.swing.interop.SwingInterOpUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static classes_techniques.EtatBorne.disponible;
import static modeles.M_Gestion.typeClient;

public class M_Reservation {

    //public static void main(String[] args) {

    //}


    public static boolean verifImmat(String inMat){
        if(inMat.matches("^[A-Z]{2}[-][0-9]{3}[-][A-Z]{2}$") || inMat.matches("^[0-9]{3}[' '][A-Z]{3}[' '][0-9]{2}$")) {
            return true;
        } else {
            System.out.println("Format immatriculation fausse.\nAttendu : 'AA-000-AA' ou '000 AAA 00'\nVeuillez réessayer.");
            return false;
        }
    }


    public static void reserverBorne(Utilisateur utilisateur, String plaque) throws IOException, ParseException {
        System.out.println("Vous effectuez une réservation.");

        //recuperation et verification date et durée
        LocalDateTime debut = recuperationDebut();
        while(debut.isBefore(LocalDateTime.now())){
            System.out.println("Cette date est invalide : elle est déjà passée.");
            debut = recuperationDebut();
        }
        LocalDateTime fin = recuperationFin(debut);
        System.out.println("Début : " + debut + "\nFin : " +fin);

        //---- recherche borne(s) disponible(s) ----
        //verification type client

        switch(utilisateur.getTypeutil()){
            case abonne:
                Borne borneDispo=verifBorneAbonne(utilisateur.getContrats());
                if(borneDispo!=null){
                    System.out.println("Borne abonné : " + borneDispo.getId());
                    break;
                }
            default:
                System.out.println("coucou");
        }

        System.out.println();


        //---- ajout BDD ----
        //LocalDateTime datedebTest =  LocalDateTime.parse("2022-06-06 12:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        //LocalDateTime datefinTest =  LocalDateTime.parse("2022-06-06 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        //ajoutBDDResa(datedebTest, datefinTest, 1, 2);


    }



    public static LocalDateTime recuperationDebut() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Veuillez entrer la date à laquelle vous souhaitez réserver. [Format jj/mm/aaaa]");

        String inDate = in.readLine();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M/d/yyyy");
        LocalDate date = LocalDate.parse(inDate, dateFormat);

        System.out.println("Veuillez entrer une heure de début. [Format hh:mm]");
        String inHeure = in.readLine() + ":00";
        LocalTime heure = LocalTime.parse(inHeure);

        LocalDateTime debut = LocalDateTime.of(date, heure);
        System.out.println(debut);
        return debut;
    }


    public static LocalDateTime recuperationFin(LocalDateTime debut) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Choisissez une durée de reservation :");
        System.out.println("[15] 15 mins\n[30] 30 mins\n[45] 45 mins\n[60] 1 heure");
        System.out.println("[90] 1h30\n[120] 2 heures\n[180] 2h30\n[240] 3 heures\n");
        boolean good=false;
        while(good==false){
            switch(in.readLine()){
                case "15":
                    return debut.plusMinutes(15);
                case "30":
                    return debut.plusMinutes(30);
                case "45":
                    return debut.plusMinutes(45);
                case "60":
                    return debut.plusMinutes(60);
                case "90":
                    return debut.plusMinutes(90);
                case "120":
                    return debut.plusMinutes(120);
                case "180":
                    return debut.plusMinutes(180);
                case "240":
                    return debut.plusMinutes(240);
                default:
                    System.out.println("erreur, veuillez choisir une valeur proposée.");
            }
        }
        return null;
    }

    public static Borne verifBorneAbonne(List<Contrat> contrats){
        System.out.println("entrée dans verifBorneAbonne");
        for (Contrat contrat:contrats) {
            System.out.println("Contrat ! Borne : " + contrat.getBorne().getId() + " etat : " + contrat.getBorne().getEtat());
            if (contrat.getBorne().getEtat()==disponible){
                return contrat.getBorne();
            }
        }
        return null;
    }

    public static void viewReservations(Utilisateur currentUser) throws IOException, ParseException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(currentUser.voirReservations());
        System.out.println("Entrez :\n   [a] Ajouter une réservation\n   [m] Modifier une réservation\n   [p] Prologer ma réservation en cours\n   [s] Supprimer une réservation");
        String choix = in.readLine();
        switch (choix){
            case "a" :
            case "A" :
                System.out.println("ajout reservation\n Entrez la plaque : ");
                reserverBorne(currentUser, in.readLine());
                break;
            case "m" :
            case "M" :
                System.out.println("Modification reservation");
                System.out.println("Quelle réservation modifier ? Entrez le numéro de la réservation");
                String modif = in.readLine();
                boolean ok=false;
                while(ok!=true) {
                    if (currentUser.getIdResa().contains(Integer.valueOf(modif))) {
                        modifierDebReservation(Integer.valueOf(modif));
                        ok = true;
                    } else {
                        System.out.println("Quelle réservation modifier ? Entrez le numéro de la réservation");
                        modif = in.readLine();
                    }
                }
                break;
            case "p" :
            case "P" :
                System.out.println("Prolonger reservation");
                break;
            case "s" :
            case "S" :
                System.out.println("Quelle réservation supprimer ? Entrez le numéro de la réservation");
                String supp = in.readLine();
                boolean ok=false;
                while(ok!=true){
                    if(currentUser.getIdResa().contains(Integer.valueOf(supp))){
                        supprimerReservation(Integer.valueOf(supp));
                        ok=true;
                    }
                    else{
                        System.out.println("Quelle réservation supprimer ? Entrez le numéro de la réservation");
                        supp = in.readLine();
                    }
                }
                break;
            case "q" :
            case "Q" :
                System.out.println("Retour");
                break;
            default:
                System.out.println("Entrée non valide, veuillez réessayer.");
        }
    }


    public static void ajoutBDDResa(Date dateDebut, Date dateFin, int idBorne, int idUtilisateur){
        Connection connection=null;

        try {
            connection=M_Bdd.connexion();
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        String type = TypeUtilisateur.abonne.toString();
        String requete = "INSERT INTO reservation (`dateDebut`, `dateFin`, `idBorne`, `idUtilisateur`) VALUES ("+dateDebut+","+dateFin+",?,?); ";
        try{
            PreparedStatement prepare = connection.prepareStatement(requete);
            prepare.setInt(1, idBorne);
            prepare.setInt(2, idUtilisateur);
            prepare.executeUpdate();
        }catch (SQLException e) {e.printStackTrace();}
        System.out.println("reservation effectuée !");
    }


    public static void supprimerReservation(int idresa){
        Connection connection=null;

        try {
            connection=M_Bdd.connexion();
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        String type = TypeUtilisateur.abonne.toString();
        String requete = "DELETE FROM reservation WHERE idresa= ?";
        try{
            PreparedStatement prepare = connection.prepareStatement(requete);
            prepare.setInt(1, idresa);
            prepare.executeUpdate();
        }catch (SQLException e) {e.printStackTrace();}
        System.out.println("reservation annulée !");

    }

    public static void modifierDebReservation(int idresa){
        Connection connection=null;

        try {
            connection=M_Bdd.connexion();
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        String type = TypeUtilisateur.abonne.toString();
        String requete = "UPDATE reservation SET dateDebut = ? WHERE idresa =  ?";
        try{
            PreparedStatement prepare = connection.prepareStatement(requete);
            prepare.setInt(1, idresa);
            prepare.setInt(2, idresa);
            prepare.executeUpdate();
        }catch (SQLException e) {e.printStackTrace();}
        System.out.println("reservation annulée !");

    }



}
